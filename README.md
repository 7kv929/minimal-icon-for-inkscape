# Minimal Icon for Inkscape
#### How to install?

 - Extract zip file, Copy *Minimal Icon* folder.

 - Open Inkscape.

 - Find your Inkscape user icon folder by navigating to **Edit > Preferences > System > User icons** or **Edit > Preferences > Interface > Theme > User icons**

 - open User icons folder

 - Paste the *Minimal Icon* folder into that Inkscape icon folder.

 - Restart inkscape

 - Change icon pack :Edit > Preferences > Interface > Theme > User icons

